const Gtk = imports.gi.Gtk;
const Glib = imports.gi.GLib;

//print('hello');

Gtk.init( null, null );

// inicializar el objeto
let win = new Gtk.Window({type: Gtk.WindowType.TOPLEVEL});

// set the window title
win.title = "Hola wey, js + GTK rocks!!!"
win.connect("destroy", function(){
	print("El usuario quiere acabar la app, bye!!!");
	Gtk.main_quit();
});

win.show();

// start the main thread
Gtk.main();
